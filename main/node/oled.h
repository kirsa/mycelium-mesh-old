/**
* @file oled.h
* @brief Functions for an OLED display
*/

#pragma once
using namespace std;
/*
extern "C" {
}
*/

extern struct SSD1306_Device I2CDisplay;

void oledSetup( struct SSD1306_Device* DisplayHandle, const struct SSD1306_FontDef* Font );
void oledPrint( struct SSD1306_Device* DisplayHandle, const char* text );

bool DefaultBusInit( void );
