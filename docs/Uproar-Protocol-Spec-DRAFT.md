# **mycelium Protocol Specification (Draft)**

## **Introduction**

Corporate Internet and cellular networks are high performance and have widespread access, but they cannot be relied on. Internet and phone access can and often is unavailable when we need it most, due to natural disasters or interference from authorities. The danger of being cut off from basic communications poses a threat to the safety, autonomy, and self-determination of **all people.**[^1]

<br/>
<br/>

## **Design Priorities:**
The following are the project design priorities which are relevant to the protocol:
* Regional: While Mycelium networks can cover large areas using well-placed Relay Nodes, Mycelium networks are intented to be used within a region, such as a metropolitan area, or in a portion of a forest during an occupation. Region-to-Region communications are assumed to be facilitated using other means.
* Asynchronous: Messages should be cached in the network for users, in case they are offline or out of coverage area.
* Attack-resistant: The functionality of the network should be resistant to attempts to disrupt functionality.
* Low Power: Nodes cannot depend on the power grid, they must be able to run on modest solar, wind or battery power.
* Range over speed: While WiFi mesh networks prioritize high-speed access in dense local environments, Mycelium is designed to cover much longer distances at lower speeds.
* Privacy: The network should be encrypted and communications should be authenticated.
* Sustainability: Upkeep for the network should be low-effort, and it should be maintainable by non-experts.
* Multilingual: We should ensure that multilingual character sets are used every step of the way, furthermore... any text-compression used should be useful in multiple languages.
* Content-Based Network: At least for now, end-to-end addressing doesn't really matter ¯\_(ツ)_/¯
* Modular: While mycelium is being developed with LoRa PHY in mind. Eventually, it should be able to be used with nearly any other [digital modulation.](https://en.wikipedia.org/wiki/Modulation#List_of_common_digital_modulation_techniques)

<br/>
<br/>

## **Nodes**
Information is propagated throughout the network from Node to Node.
Two types of Node exist, Relay Nodes and Peer Nodes.

**Relay Nodes**
Relay nodes are presumed to be stationary nodes which are responsible for setting up and maintaining a mesh with eachother, and using the mesh to propagate messages sent to them by the Peer Nodes.
They are also responsible for storing all messages they receive for immediate or later retreival (up to a few days) by Peer Nodes.

**Peer Nodes**
Peer Nodes can be mobile or stationary. Either a personal device carried by a single person, or set up in a vehicle, house, or public space for multiple end-users to connect to. Peer Nodes are NOT responsible for storing all messages they receive like Relay Nodes are, in-fact, a Relay Node will only send a Peer Node messages which the Peer Node has asked for. Eventually, Peer Nodes should be able to communicate directly with each-other, or use a neighboring Peer Node as an intermediary to contact the nearest Relay.

<br/>
<br/>

## **Layers**

* **PHY Layer:** Relates to LoRa PHY itself, and it's modulation parameters.

* **Network Layer:** This is the layer responsible for accepting, (de/en)crypting, storing, and disseminating messages.

* **Control Layer** Metadata, Encryption and settings influencing routing decisions happen at this layer

* **Message Layer:** Compression and Formatting happen at this layer, these are the messages humans read and interact with.

<br/>

### PHY Layer

PHY Layer is LoRa PHY (Not to be confused with LoRaWAN, which is an IoT-focused centralized protocol which is not used in this project)

<br/>

### Network Layer

**Network-Layer Security**

Link level security between Relays is handled by a symmetric cipher (ChaCha20-ietf), using a 128-bit key, a 32-bit counter, and a 96-bit nonce
Link level security between Relays and Peers, is handled by a different symmetric cipher.

Eventually, we may want to have the ability to add and delete singular Peer and Relay nodes from the network using asymmetric key encryption and some sort of registration to receive the latest symmetric key cipher for the network.


**Relay-Relay Network:** The mycelium Core network is broadcast-only, and therefore does not carry a destination address in the header. The default method of propagating messages is through broadcasts, and upon receipt a message is hashed and the relay will transmit an acknowledgement of the message. The ACK is to prevent superfluous broadcasts. <br/>
Each relay maintains a table of neighboring relays, and upon receipt of a message intended for propagation, the relay node will expect to hear that message, or the acknowledgement of that message from each relay in it's neighbor table, after a random period of time if it does not, it will re-broadcast the message n-times until it hears the message or acknowledgement from each of it's neighbors. This behavior is intended to prevent unnecessary broadcasts which waste bandwidth and power.

**Relay-Peer Network:**

Solicits a connection with any Relay which is within range. Relays can differentiate between a Peer and another Relay by reading the Protocol Register at the front of the packet, they will use this information to apply the correct symmetric decryption key to read the message. Upon connection with the Relay, the Peer will request any updates on any subscribed message-groups (chat groups), the Peer will request the updates using absolute (in the case that the network operates on absolute time) or relative time (new updates since x seconds ago).

Depending on administrator settings (of any given Relay Node), when the Peer and Relay have maintained a stable connection, the Relay Node may push updates to the Peer unasked. Otherwise the Peer will need to contact the Relay periodically for new updates. The Relay will be configured to ignore the Peer if the requests are too frequent.

**Peer-Peer Network:**

Future use

<br/>

### Control Layer

End users can participate in user-to-user chats, and group chats using this layer. Chats are established using asymmetric encryption between all endpoints where a shared symmetric key is established. Messages are not addressed in an endpoint-to-endpoint manner but rather unique (chat) group identifiers are used as an address which all members of that group send their messages to. Additionally each user will have a unique identifier generated on first boot, to which it will subscribe to (listen), when connected to a Relay. In order to contact someone directly on the network one must know that user's unique identifier.

<br/>

### Message Layer

Compression and Formatting happen at this layer, these are the messages humans read and interact with.
These messages can also be used for disseminating network data and sending/receiving administrative information.

<br/>

## **Mycelium Packets**

Initially the header for Mycelium packets will be the full 18-Bytes long, future functionality will include header compression by removing the most significant bytes of the Message Counter and Node ID field.

<br/>

### **(Smallest possible) Header**
The smallest possible header is 7-Bytes long.
```
|-------------------Header----------------------|
                              |--------------------ChaCha20 Encryption---------------------->
|-PR- |NL|ML|---MC---|--NID---|----------------MAC----------------|--Type--|--------Payload---------->
 □□□□  □□ □□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ ■■■■■■■■ ■■■■■■■■ ■■■■■■■■
□ = Unencrypted bit
■ = Encrypted bit

Header:                                
* Protocol Register(PR)              4-bits             Tells the receiver how to interpret the rest of the packet
* NL  (NID Length)                   2-bits             Indicates number of Bytes in the Node Identifier field.
* ML  (MC Length)　                  2-bits             Indicates number of Bytes in the Message Counter field.
* MC  (Message Counter)              1 - 4 Bytes        Total number of messages sent by sending node since first boot.
* RID (Node Identifier)              2 - 8 Bytes        Unique identifier for each Node on the local Mycelium Network
* MAC                                4 Bytes            Message Authentication Code
* Type                               8-bits             Indicates the type of payload
Payload:
* Metadata and Content               0-232 Bytes        Max payload length is 232 Bytes.
```

<br/>

### **(Largest possible) Header**
The largest possible header is 18-Bytes long.
```
|-------------------------------------------------------------Header------------------------------------------------------------------------------|
                                                                                                                                |----ChaCha20 Encryption---->
|-PR-|NL|ML|---PR---|-----------Message Counter---------|------------------------------------Node ID----------------------------|----------------MAC----------------|------Type-------|-----Payload---->
 □□□□ □□ □□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ ■■■■■■■■ ■■■■■■■■ ■■■■■■■■
□ = Unencrypted bit
■ = Encrypted bit

Header:                                
* Protocol Register(PR)              4-Bits + expansion Byte      Tells the receiver how to interpret the rest of the packet
* NL  (NID Length)                   2-bits                       Indicates number of Bytes in the Node Identifier field.
* ML  (MC Length)                    2-bits                       Indicates number of Bytes in the Message Counter field.
* MC  (Message Counter)              4 Bytes                      Total number of messages sent by sending node since first boot.
* NID (Node Identifier)              8 Bytes                      Unique identifier for each Node on the local Mycelium Network
* MAC                                4 Bytes                      Message Authentication Code                     
* Type                               2 Bytes                      Indicates the type of payload
Payload:
* Metadata and Content               0-232 Bytes                  Max payload length is 232 Bytes.
```

<br/>

#### Protocol Register (PR)
Shares the first byte in the packet with the NID Length and MC Length

```
□                   bit 7                   Expansion bit, set to 1 to extend size of the Protocol Register
□□□                 bits 6-4                Protocol Register Number, 0-7
□□□□ + □□□□□□□□     bits 7-4 & 2nd Byte     PR With extension-byte.

```

* 0.000(0)  =   Relay<->Relay Packet
* 0.001(1)  =   Relay<->Peer Packet
* 0.010(2)  =   Peer<->Peer Packet (Unused)

<br/>

#### NID Length
Shares the first byte in the packet with the Protocol Register & MC Length
This field indicates the length of the Node Identifier field. The Node Identifier itself
does not change size, but other Nodes keep track of Nodes they have seen and can inter
the full Node Identifier using less than the entire 8-Bytes of the NID.

```
□□      bits 3-2    Indicates 1 - 8 bytes used for expressing the NID

```

* 00 - 1-Byte NID Length (Relay)
* 01 - 2-Byte NID Length (Relay)
* 10 - 4-Byte NID Length (Relay)
* 11 - 8-Byte NID Length (Relay)




<br/>

#### MC Length
Shares the first byte in the packet with the Protocol Register & NID Length
Similar to the NID Length, the Message Counter is a permanant counter which always
increases by one after each sent message. Neighboring Nodes will keep track of their
neighbor's counters and infer the whole counter value using the least-significant-Bytes
contained in the Message Counter Field.

```
□□      bits 1-0    Indicates 1 - 4 bytes used for expressing the Message Counter

```
* 00 - 1-Byte Message Counter
* 01 - 2-Byte Message Counter
* 10 - 3-Byte Message Counter
* 11 - 4-Byte Message Counter



<br/>

### Node Identifier (NID)
NID is based on the 16-Byte hash of the node's public keys which is generated on first-boot, once adjacent Nodes remember the full NID,
the abbreviated NIDs could be used for most future transmitted packets.

Both Relay Nodes, and Peer Nodes have Node Identifiers (NID).

When possible, a Relay will use the shortest-possible NID. For example, if the least-significant-byte (LSB) of a node's NID is "9F", and no other Nodes have a NID where the LSB is "9F",
then it can use that single-byte abbreviated NID most of the time.

Relays will supersede Peers when it comes to NID Abbreviation. For example, if a Peer has "C2" as it's NID's LSB, and a Relay joins the network, and their NIDs LSB is also "C2", then they will steal that abbreviation and the Peer will have to use a 2 Byte abbreviated NID from that point onward.

```
Full NID:
□□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□

Abbreviated NID:
□□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□

Abbreviated NID:
□□□□□□□□ □□□□□□□□

Abbreviated NID:
□□□□□□□□


```

<br/>

### Message Authentication Code (MAC)
The MAC consists of the 4 most significant bytes(MSB) of an 8-byte cryptographic hash of the header (with complete MC & NID, not abbreviated), the hash algorithm is SipHash-2-4, and a pre-shared secret key is used to make the hash into an authentication code.

```
Message Authentication Code:
□□□□□□□□ □□□□□□□□ □□□□□□□□ □□□□□□□□

```

<br/>

### Type
This is where the Network Layer ends and the Control Layer begin.
The Type field tells the Relay or Peer how to handle the message

```
■■■■■■■■   bits 7-0   Value may be 0-255

```

* 00000000(0) = CRC-only Payload
* 00000001(1) = NetMGMT Hello
* 00000010(2) = NetMGMT Update
* 00000011(3) = NetMGMT Reserved
*  ....    
* 00001111(15) = NetMGMT Reserved
* 00010000(16) = NetSRV Set-Mod
* 00010001(17) = NetSRV ID-Update
* 00011111(18) = NetSRV Timesync
* 00100000(19) = NetSRV Reserved
*  ....
* 00111111(63) = NetSRV Reserved
* 01000000(64) = Uncompressed, Unencrypted message
* 01000000(65) = Compressed, Unencrypted message
* 01000000(66) = Compressed, Encrypted message
* 01000000(67) = Group Invite message
<br/>

### Payload
Application data goes here, limited to 232 Bytes.

<br/>
<br/>
<br/>

------------


[^1]: **Communications disruptions by governments and other authorities**

    * **United Kingdom:** In the United Kingdom, the Communications Act 2003 and the Civil Contingencies Act 2004 allow the Secretary of State for Culture, Media and Sport to suspend Internet services, either by ordering Internet service providers to shut down operations or by closing Internet exchange points.

    * **United States:** On March 9, 2006, the National Communications System (‘NCS’) approved SOP 303, however it was never released to the public. This secret document codifies a ‘shutdown and restoration process for use by commercial and private wireless networks during national crisis.’ After a Bay Area Rapid Transit (“BART”) officer in San Francisco shot and killed a homeless man named Charles Hill on July 3, 2011. The shooting sparked massive protests against BART throughout July and August 2011. During one of these protests, BART officials cut off cell phone service inside four transit stations for three hours. This kept anyone on the station platform from sending or receiving phone calls, messages, or other data. The US Marshals Service has (since 2014 at latest) flown Cessna aircraft outfitted with stingray devices that mimic cellular towers, allowing them to spy on and disrupt cellular communications.[<sup>Guardian</sup>](https://www.theguardian.com/world/2014/nov/14/government-planes-mimic-cellphone-towers-to-collect-user-data-report) During the Dakota Access Pipeline protests in 2016 and 2017, Cellular and WiFi service was disrupted by authorities, *"People from every corner, whether they had been on site for days or for months, would talk about their cellphone signals cutting out just as drones circled above. Mobiles would switch themselves off and on again -- not in pocket but in hand. Camera apps were opened out of nowhere, and batteries would drain by enormous percentages, killing the phones in minutes"*[<sup>Cracked</sup>](https://www.cracked.com/personal-experiences-2418-the-standing-rock-hacks-cracked-unravels-real-conspiracy.html) [<sup>EFF</sup>](https://www.eff.org/deeplinks/2016/12/investigating-law-enforcements-use-technology-surveil-and-disrupt-nodapl-wate)

    * **China:** China has completely shut down Internet service in the autonomous region of Xinjiang for almost a year after the July 2009 Ürümqi riots.

    * **Egypt:** On January 27, 2011, during the Egyptian Revolution of 2011, the government of President Hosni Mubarak cut off access to the Internet by all four national ISPs, and all mobile phone networks.

    * **Zimbabwe:** On January 15, 2019, internet monitoring group NetBlocks reported the blocking of over a dozen social media platforms in Zimbabwe followed by a wider internet blackout amid protests over the price of fuel.

    * **India:** By far, India and it's states make the most extensive use of the [Communications Blackouts](https://en.wikipedia.org/wiki/Internet_censorship_in_India#Usage_of_Internet_kill_switch]) to control political dissidents. Before revocation of autonomous status of Jammu and Kashmir (presently under threat of [genocide](https://www.genocidewatch.com/single-post/2019/08/15/Genocide-Alert-for-Kashmir-India)), the Internet services were shut down as part of curfew and complete communications blackout including cable TV, landlines and cellphones on 4 August 2019. The Internet and other communication services have still not been restored. In the latter part of 2019, in response to mass protests across India, the national and state governments had cut off communications in dozens of cities, affecting tens of millions of people. [<sup>[nytimes]</sup>](https://www.nytimes.com/2019/12/17/world/asia/india-internet-modi-protests.html)
