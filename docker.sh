#!/bin/bash

while getopts "mhp:d:" opt; do
  case $opt in
    p) USB_PORT=$OPTARG ;;
    d) PROJECT_DIR=$OPTARG ;;
    m) OPERATION="monitor" ;;
    h) HELP="help" ;;
  esac
done

if [[ "${HELP}" ]]; then
  echo "=Docker Build/Flash Helper="
  echo "This script uses the current docker build environment to build your project code, flash it to your ESP device, and display monitoring output."
  echo ""
  echo "Usage: $0 [options]"
  echo "-p USB_PORT : Set the path to your USB device (default /dev/ttyUSB0)"
  echo "-d PROJECT_DIR : Set the path to your project directory (default ~/esp/myceliumesp-cpp)"
  echo "-m : Don't build or flash the code, just monitor a running device's output"
  echo "-h : Display this help"
  exit
fi

if [[ -z "${OPERATION}" ]]; then
  OPERATION="build"
fi

if [[ -z "${USB_PORT}" ]]; then
  USB_PORT="/dev/ttyUSB0"
fi

if [[ -z "${PROJECT_DIR}" ]]; then
  PROJECT_DIR="$HOME/esp/myceliumesp-cpp"
fi

echo "Input your Gitlab account credentials:"
docker login registry.gitlab.com
docker container rm esp-build-`basename $USB_PORT` || return 1
echo "Press CTRL+] to exit monitor mode"
docker run -it --device=$USB_PORT -v $PROJECT_DIR:/project -e USB_PORT -w /project --name esp-build-`basename $USB_PORT` registry.gitlab.com/raddevs/esp-build:dev "./$OPERATION.sh"
