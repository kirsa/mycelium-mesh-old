/**
* @file config.h
* @brief Set configuration values like address, keys etc
*  eventually everything should be configurable via menuconfig.
*/

#pragma once
using namespace std;
extern uint8_t nodeType;
extern uint8_t key[32];
extern uint8_t sipKey[24];
extern int64_t deadTimer; //10 minutes

extern bool verbose;
