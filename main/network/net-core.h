/**
* @file net-core.h
* @brief Data structures for packets/datagrams and functions
*  for transmitting and receiving packets/datagrams, as well
*  as a decision tree-like function for passing received datagrams
*  to the proper external functions for further processing
*/
#pragma once
using namespace std;
#include <forward_list>
#include <map>
#include <vector>
#include <queue>
#include "../node/crypto.h"
#include "lora.h"
#include "config.h"

extern bool txLock;
#define REG_PAYLOAD_LENGTH        0x22
#define REG_MODEM_STAT						0x18

//extern map<uint8_t, forward_list<node>> nodesList;
class packet;
extern queue<packet>tx_buffer;

union u8_16t{
    uint16_t u16;
    uint8_t u8[2];
};
union u8_32t{
    uint32_t u32;
    uint8_t u8[4];
};
union u8_64t{
    uint64_t u64;
    uint8_t u8[8];
};

/**
* @brief Large "packet" class responsible for tx/rx of packets.
*/

class packet {
  public:
    uint8_t type;
    uint8_t type_ext;
    u8_64t nid;
    vector<uint8_t> payload;
    vector<uint8_t> datagram;

    void send(vector<uint8_t> payload, uint8_t type, uint8_t type_ext = 0);
    void receive();
    void rx_process_payload();
    void show_tx_packet();
    void show_queue_packet();
    void show_rx_packet();

  private:
    uint8_t prx;
    uint8_t proto;
    uint8_t mc_len;
    uint8_t nid_len;
    uint8_t init_byte;
    uint8_t pr_ext;
    u8_32t counter;
    uint8_t nonce[12];
    u8_32t header_hash;
    vector<uint8_t> header;
    uint8_t nid_bytes;

    const uint TYPE_NetMGMT_Hello = 1;

    void tx_prepare_initbyte();
  	void tx_prepare_header();
    void tx_increment_counter();
  	void tx_prepare_nonce();
  	void tx_prepare_hash();
  	void tx_prepare_payload();
    void tx_encrypt_payload();
    void tx_prepare_packet();

    void rx_atomize_header();
    void rx_extract_payload();
    void rx_prepare_nonce();
    void rx_reassemble_header();
    bool rx_verify_header_hash();
    void rx_decrypt_payload();
};

void tx_transmit();
