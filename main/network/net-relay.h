/**
* @file network-relay.h
* @brief Functions for managing relay nodes,
*  neighbor tables, message storage, etc.
*/

#pragma once
using namespace std;
#include "../node/crypto.h"
#include <iostream>
#include <map>

extern map<uint64_t, int64_t> neighborTable;
    void netMGMT_addNeighbor(uint64_t nid);
    void netMGMT_pruneNT();
    void netMGMT_txHello();
    void netMGMT_rxHello();
