/**
* @file pm.h
* @brief Power Management & Power Saving functionality
*/

#pragma once


#define MHZ     1000000


void autosleep_setup();
void autosleep_hold();
void autosleep_release();
