#!/bin/bash
. $HOME/esp/esp-idf/export.sh
idf.py build && \
idf.py -p $USB_PORT flash && \
idf.py -p $USB_PORT monitor
